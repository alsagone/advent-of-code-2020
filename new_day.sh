#!/bin/bash

function myTouch {
    for i in $@
    do
        if [ ! -f "$i" ]; then
            touch "$i"
            echo "$i"
        else
            echo "$i existe déjà"
        fi 
    done
}

if [ $# -eq 1 ]; then
    day="$1"
else
    echo -n "Day: "
    read day 
fi

myTouch "inputs/day-${day}.txt" "code/adc2020-day_${day}.py"