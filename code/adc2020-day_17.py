from collections import Counter

active = '#'
inactive = '.'


def getPuzzleInput() -> list[str]:
    with open("../inputs/day-17.txt") as f:
        puzzleInput = f.read().splitlines()

    return puzzleInput


def getAllNeighbors(coordinates: tuple):
    assert len(coordinates) == 3
    neighborsList = []

    for x in range(-1, 2):
        newX = coordinates[0] + x

        for y in range(-1, 2):
            newY = coordinates[1] + y

            for z in range(-1, 2):
                neighbor = (newX, newY, coordinates[2] + z)

                if (neighbor != coordinates):
                    neighborsList.append(neighbor)

    return neighborsList


def parseInput():
    puzzleInput = getPuzzleInput()
    puzzleDict = {}

    for lineIndex, line in enumerate(puzzleInput):
        for valueIndex, value in enumerate(line):
            coordinates = (lineIndex, valueIndex, 0)
            if (value != active) and (value != inactive):
                print(value)
                raise ValueError

            puzzleDict[coordinates] = value

    return puzzleDict


def playGame():
    puzzleDict = parseInput()

    for _ in range(6):
        newActiveCubesCoordinates = []
        newInactiveCubesCoordinates = []

        # Expand the grid
        for coordinates, status in list(puzzleDict.items()):
            for neighbor in getAllNeighbors(coordinates):
                if neighbor not in puzzleDict.keys():
                    puzzleDict[neighbor] = inactive

        # Count the number of active neighbors
        for coordinates, status in list(puzzleDict.items()):
            nbActiveNeighbors = 0

            for neighbor in getAllNeighbors(coordinates):
                try:
                    if (puzzleDict[neighbor] == active):
                        nbActiveNeighbors += 1

                except KeyError:
                    pass

            # If a cube is active and exactly 2 or 3 of its neighbors are also active, the cube remains active.
            # Otherwise, the cube becomes inactive.
            if (status == active):
                if (nbActiveNeighbors != 2) and (nbActiveNeighbors != 3):
                    newInactiveCubesCoordinates.append(coordinates)

            # If a cube is inactive but exactly 3 of its neighbors are active, the cube becomes active.
            # Otherwise, the cube remains inactive.
            elif (status == inactive):
                if (nbActiveNeighbors == 3):
                    newActiveCubesCoordinates.append(coordinates)

            else:
                print(status)
                raise ValueError

        for c in newActiveCubesCoordinates:
            puzzleDict[c] = active

        for c in newInactiveCubesCoordinates:
            puzzleDict[c] = inactive

    # Count the number of active cubes
    count = Counter(puzzleDict.values())

    return count[active]


def partOne():
    print(f"Part one: {playGame()}")


def getAllNeighbors_4D(coordinates):
    assert len(coordinates) == 4
    neighborsList = []
    for x in range(-1, 2):
        newX = coordinates[0] + x

        for y in range(-1, 2):
            newY = coordinates[1] + y

            for z in range(-1, 2):
                newZ = coordinates[2] + z

                for w in range(-1, 2):
                    neighbor = (newX, newY, newZ, coordinates[3] + w)

                    if (neighbor != coordinates):
                        neighborsList.append(neighbor)

    return neighborsList


def parseInput_4D():
    puzzleInput = getPuzzleInput()
    puzzleDict = {}

    for lineIndex, line in enumerate(puzzleInput):
        for valueIndex, value in enumerate(line):
            coordinates = (lineIndex, valueIndex, 0, 0)
            if (value != active) and (value != inactive):
                print(value)
                raise ValueError

            puzzleDict[coordinates] = value

    return puzzleDict


def playGame_4D():
    puzzleDict = parseInput_4D()

    for _ in range(6):
        newActiveCubesCoordinates = []
        newInactiveCubesCoordinates = []

        # Expand the grid
        for coordinates, status in list(puzzleDict.items()):
            for neighbor in getAllNeighbors_4D(coordinates):
                if neighbor not in puzzleDict.keys():
                    puzzleDict[neighbor] = inactive

        # Count the number of active neighbors
        for coordinates, status in list(puzzleDict.items()):
            nbActiveNeighbors = 0

            for neighbor in getAllNeighbors_4D(coordinates):
                try:
                    if (puzzleDict[neighbor] == active):
                        nbActiveNeighbors += 1

                except KeyError:
                    pass

            # If a cube is active and exactly 2 or 3 of its neighbors are also active, the cube remains active.
            # Otherwise, the cube becomes inactive.
            if (status == active):
                if (nbActiveNeighbors != 2) and (nbActiveNeighbors != 3):
                    newInactiveCubesCoordinates.append(coordinates)

            # If a cube is inactive but exactly 3 of its neighbors are active, the cube becomes active.
            # Otherwise, the cube remains inactive.
            elif (status == inactive):
                if (nbActiveNeighbors == 3):
                    newActiveCubesCoordinates.append(coordinates)

            else:
                print(status)
                raise ValueError

        for c in newActiveCubesCoordinates:
            puzzleDict[c] = active

        for c in newInactiveCubesCoordinates:
            puzzleDict[c] = inactive

    # Count the number of active cubes
    count = Counter(puzzleDict.values())

    return count[active]


def partTwo():
    print(f"Part two: {playGame_4D()}")


if __name__ == "__main__":
    partOne()
    partTwo()
