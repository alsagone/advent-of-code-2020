import re
import sys

bag_color_pattern = re.compile("^([a-z ]+) bags contain")
bag_content_pattern = re.compile("([\d+]) ([a-z ]+) bag[s]?")
bag_rule_pattern = re.compile("contain ([^.]+)")

rules_dict = {}


def getInput():
    file = '../inputs/day-07.txt'

    with open(file) as f:
        lines = f.read().splitlines()

        for l in lines:
            bag_color = re.search(bag_color_pattern, l).group(1)
            bag_rule = re.search(bag_rule_pattern, l).group(1)
            rules_dict[bag_color] = bag_rule


def getBagContent(color):
    try:
        rule = rules_dict[color]
    except KeyError:
        print(f'Error: unknown color "{color}"', file=sys.stderr)
        sys.exit(1)

    content = {}

    if not("no other" in rule):
        rule_description = re.findall(bag_content_pattern, rule)

        for r in rule_description:
            number_bags = int(r[0])
            bag_color = r[1]
            content[bag_color] = number_bags

    return content


def mergeBag(bag, other_bag):
    merged_bag = bag.copy()
    for color, quantity in other_bag.items():
        try:
            merged_bag[color] += quantity

        except KeyError:
            merged_bag[color] = quantity

    return merged_bag


def contains(bag, color):

    # if the color we search is directly in the bag
    if (color in bag.keys()):
        return True

    unzippedBag = bag.copy()
    unzipped = False

    while not(unzipped):
        unzipped = True
        for k in list(unzippedBag.keys()):
            content = getBagContent(k)

            # if the color we search is directly in the content
            if (color in content.keys()):
                return True

            if (len(content) > 0):
                unzipped = False

                # for each bag B in content:
                # 1 - check if B directly holds the bag color we search by checking the rules
                # 2 - else call recursively this function with the content of B
                for key in content.keys():
                    if (color in rules_dict[key]) or (contains(getBagContent(key), color)):
                        return True

                quantity = unzippedBag.pop(k)

                # example: {"green": 4}
                # if unfolding "green" once gives {"blue": 7, "red": 3}
                # then unfolding "green" four times gives {"blue": 28, "red": 12}
                # dict["blue"] = dict["blue"] * 4
                # dict["red"] = dict["red"] * 4

                if (quantity > 1):
                    for key in content.keys():
                        content[key] *= quantity

                unzippedBag = mergeBag(unzippedBag, content)

    return False


def countBags(bag):
    count = 0

    for color, quantity in bag.items():
        content = getBagContent(color)
        count += quantity + quantity * countBags(content)

    return count


def partOne():
    count = 0

    for color in rules_dict.keys():
        if (color != 'shiny gold') and (contains(getBagContent(color), 'shiny gold')):
            count += 1

    print(f"Part one: {count}")


def partTwo():
    shiny_gold = getBagContent('shiny gold')
    count = countBags(shiny_gold)
    print(f"Part two: {count}")


if __name__ == "__main__":
    getInput()
    partOne()
    partTwo()
