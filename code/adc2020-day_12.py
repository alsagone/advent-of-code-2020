import math
import re
import sys

global direction_pattern
direction_pattern = re.compile("([EFLNRSW]{1})(\d+)")

global current_position
current_position = {'E': 0, 'W': 0, 'N': 0, 'S': 0}

global current_dir
current_dir = 'E'


def getInput():
    with open('../inputs/day-12.txt') as f:
        lines = f.read().splitlines()

    return lines


global input
input = getInput()


def parseCommand(command):
    search = re.search(direction_pattern, command)

    if (search == None):
        print(f"Invalid command: {command}", file=sys.stderr)
        sys.exit(1)

    return search.group(1), int(search.group(2))


def move(command):
    movement, value = parseCommand(command)
    global current_dir

    if (movement == 'F'):
        current_position[current_dir] += value

    elif (movement in "LR"):
        new_directions = "ENWS" if (movement == 'L') else "ESWN"
        offset = value // 90
        new_dir_index = (new_directions.index(current_dir) + offset) % 4
        current_dir = new_directions[new_dir_index]

    elif (movement in "NSEW"):
        current_position[movement] += value

    else:
        print(f'Error - unknown movement "{movement}', file=sys.stderr)
        sys.exit(1)


def partOne():
    for line in input:
        move(line)

    x = current_position['E'] - current_position['W']
    y = current_position['S'] - current_position['N']
    result = abs(x) + abs(y)
    print(f"Part one: {result}")

######## PART TWO ########


global coord
coord = {'x': 0, 'y': 0}
global waypoint
waypoint = {'x': 10, 'y': 1}


def rotate(origin, point, angle):
    ox, oy = origin
    px, py = point
    qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
    qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
    return int(round(qx)), int(round(qy))


def navigate(command):
    movement, value = parseCommand(command)
    if (movement == 'N'):
        waypoint['y'] += value

    elif (movement == 'S'):
        waypoint['y'] -= value

    elif (movement == 'E'):
        waypoint['x'] += value

    elif (movement == 'W'):
        waypoint['x'] -= value

    elif (movement == 'F'):
        coord['x'] += waypoint['x'] * value
        coord['y'] += waypoint['y'] * value

    elif (movement in "LR"):
        v = value * -1 if (movement == 'R') else value

        origin = (0, 0)
        point = (waypoint['x'], waypoint['y'])
        angle = math.radians(v)
        w_x, w_y = rotate(origin, point, angle)
        waypoint['x'] = w_x
        waypoint['y'] = w_y


def partTwo():
    for line in input:
        navigate(line)

    result = abs(coord['x']) + abs(coord['y'])
    print(f'Part two: {result}')


if __name__ == '__main__':
    partOne()
    partTwo()
