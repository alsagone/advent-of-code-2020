directions = ["up", "down", "left", "right",
              "up left", "up right", "down left", "down right"]

empty = 'L'
occupied = '#'
floor = '.'
nb_rows = 0
row_length = 0


def getInput():
    input = []
    with open('../inputs/day-11.txt') as f:
        lines = f.read().splitlines()

    for l in lines:
        split = [char for char in l]
        input.append(split)

    return input


def validIndex(x, y):
    return (x >= 0) and (x < nb_rows) and (y >= 0) and (y < row_length)


def getValue(array, x, y):
    if (validIndex(x, y)):
        value = array[x][y]
    else:
        value = None
    return value


def getNeighbour(array, x, y, direction):
    a, b = x, y
    if ("up" in direction):
        a -= 1

    elif ("down" in direction):
        a += 1

    if ("left" in direction):
        b -= 1

    elif ("right" in direction):
        b += 1

    return getValue(array, a, b)


def getNbAdjacentOccupiedSeats(array, x, y):
    count = 0

    for d in directions:
        if (getNeighbour(array, x, y, d) == occupied):
            count += 1

    return count


"""
    If a seat is empty and there are no occupied seats adjacent to it, the seat becomes occupied.
    If a seat is occupied and {threshold} or more seats adjacent to it are also occupied, the seat becomes empty.
    Otherwise, the seat's status does not change.
"""


def doesTheSeatChange(status, occupied_seats, threshold):
    return (status == empty and occupied_seats == 0) or (status == occupied and occupied_seats >= threshold)


def updateSeats(rows, positions):
    for p in positions:
        x, y = p[0], p[1]
        status = getValue(rows, x, y)

        if (status == empty):
            rows[x][y] = occupied

        elif (status == occupied):
            rows[x][y] = empty


def fillSeats(rows):
    change = True

    while (change):
        change = False
        change_positions = []

        for i in range(nb_rows):
            for j in range(row_length):
                status = getValue(rows, i, j)

                if (status != floor):
                    occupied_seats = getNbAdjacentOccupiedSeats(rows, i, j)

                    if (doesTheSeatChange(status, occupied_seats, 4)):
                        change_positions.append((i, j))

        change = len(change_positions) > 0
        updateSeats(rows, change_positions)

    return rows


def countOccupiedSeats(rows):
    nb_rows = len(rows)
    row_length = len(rows[0])
    count = 0

    for i in range(nb_rows):
        for j in range(row_length):
            if (getValue(rows, i, j) == occupied):
                count += 1

    return count


def countVisibleOccupiedSeats(rows, x, y):
    count = 0
    for d in directions:
        a, b = x, y
        found = False
        while (not(found)):
            if ("up" in d):
                a -= 1

            elif ("down" in d):
                a += 1

            if ("left" in d):
                b -= 1

            elif ("right" in d):
                b += 1

            status = getValue(rows, a, b)

            # No valid seat found yet, so we continue searching
            if (status == floor):
                continue

            elif (status == occupied):
                count += 1
                found = True

            elif (status == empty):
                found = True

            # status = None -> out of bounds -> stop searching in this direction
            else:
                break

    return count


def newFillSeats(rows):
    change = True

    while (change):
        change = False
        change_positions = []

        for i in range(nb_rows):
            for j in range(row_length):
                status = getValue(rows, i, j)

                if (status != floor):
                    occupied_seats = countVisibleOccupiedSeats(rows, i, j)

                    if (doesTheSeatChange(status, occupied_seats, 5)):
                        change_positions.append((i, j))

        change = len(change_positions) > 0
        updateSeats(rows, change_positions)

    return rows


def partOne(input):
    filledInput = fillSeats(input)
    print(f"Part one: {countOccupiedSeats(filledInput)}")


def partTwo(input):
    filledInput = newFillSeats(input)
    print(f"Part two: {countOccupiedSeats(filledInput)}")


if __name__ == '__main__':
    input = getInput()
    nb_rows = len(input)
    row_length = len(input[0])

    partOne(input)

    input = getInput()
    partTwo(input)
