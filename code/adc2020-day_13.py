from functools import reduce
import re
import sys


def getInput():
    with open('../inputs/day-13.txt') as f:
        lines = f.read().splitlines()

    return lines


global input
input = getInput()


def findNextBus(busNumber, timestamp):
    t = timestamp

    while (t % busNumber != 0):
        t += 1

    return t


def partOne():
    earliestTimestamp = int(input[0])

    busNumbers = []

    for b in re.findall(r'\d+', input[1]):
        busNumbers.append(int(b))

    busDict = {}

    for b in busNumbers:
        busDict[b] = findNextBus(b, earliestTimestamp)

    earliestBus_number = 0
    earliestBus_time = sys.maxsize

    for (busNumber, busTime) in busDict.items():
        if (busTime < earliestBus_time):
            earliestBus_time = busTime
            earliestBus_number = busNumber

    waitingTime = earliestBus_time - earliestTimestamp
    answer = earliestBus_number * waitingTime
    print(f"Part one: {answer}")

# via RosettaCode


def chineseRemainder(n, a):
    sum = 0
    prod = reduce(lambda a, b: a*b, n)
    for n_i, a_i in zip(n, a):
        p = prod // n_i
        sum += a_i * mulInv(p, n_i) * p
    return sum % prod


def mulInv(a, b):
    b0 = b
    x0, x1 = 0, 1
    if b == 1:
        return 1
    while a > 1:
        q = a // b
        a, b = b, a % b
        x0, x1 = x1 - q * x0, x0
    if x1 < 0:
        x1 += b0
    return x1


def partTwo():
    inputSplit = input[1].split(',')
    remainderArray = []
    moduloArray = []

    for i in range(len(inputSplit)):
        str = inputSplit[i]

        if (str != 'x'):
            busNumber = int(str)
            modulo = busNumber
            remainder = busNumber - i

            # Make the reminder always positive
            while (remainder <= 0):
                remainder += modulo

            remainderArray.append(remainder)
            moduloArray.append(modulo)

    result = chineseRemainder(moduloArray, remainderArray)
    print(f"Part two: {result}")


if __name__ == '__main__':
    getInput()
    partOne()
    partTwo()
