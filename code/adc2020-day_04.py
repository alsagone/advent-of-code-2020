import re

passports = open("../inputs/day-04.txt").readlines()
height_pattern = re.compile("^([\d]+)(cm|in)$")
hair_color_pattern = re.compile("^#[a-f\d]{6}$")
eye_color_pattern = re.compile("^(amb|blu|brn|gry|grn|hzl|oth){1}$")
passport_id_pattern = re.compile("^[\d]{9}$")


def partOne():
    count_passports = 0
    fields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid", "cid"]
    pattern = re.compile("([a-z]+):")

    for i in range(0, len(passports)):
        match_fields = re.findall(pattern, passports[i])

        if (len(match_fields) > 0):
            for m in match_fields:
                if (m in fields):
                    fields.remove(m)

                else:
                    break

        if (i == len(passports)-1) or (len(match_fields) == 0):
            # If the program finds an empty line
            # this means, it has finished parsing the passport

            if ((len(fields) == 0) or ((len(fields) == 1) and (fields[0] == "cid"))):
                count_passports += 1

            fields = ["byr", "iyr", "eyr", "hgt",
                      "hcl", "ecl", "pid", "cid"]

    message = "Part one: {0} valid passports found.".format(count_passports)

    if (count_passports < 2):
        message = message.replace("passports", "passport")

    print(message)
    return


def checkField(field, data):
    b = False

    # cid (Country ID) - ignored, missing or not.
    if (field == "cid"):
        b = True

    # (Birth Year) - four digits; at least 1920 and at most 2002
    elif (field == "byr"):
        byr = int(data)
        b = (len(data) == 4) and (1920 <= byr) and (byr <= 2002)

    # (Issue Year) - four digits; at least 2010 and at most 2020.
    elif (field == "iyr"):
        iyr = int(data)
        b = (len(data) == 4) and (2010 <= iyr) and (iyr <= 2020)

    # (Expiration Year) - four digits; at least 2020 and at most 2030.
    elif (field == "eyr"):
        eyr = int(data)
        b = (len(data) == 4) and (2020 <= eyr) and (eyr <= 2030)

    # (Height) - a number followed by either cm or in:
    # If cm, the number must be at least 150 and at most 193.
    # If in, the number must be at least 59 and at most 76.

    elif (field == "hgt"):
        height_search = re.search(height_pattern, data)

        if (height_search != None):
            height = int(height_search.group(1))
            unit = height_search.group(2)

            if (unit == "cm"):
                b = (150 <= height) and (height <= 193)
            elif (unit == "in"):
                b = (59 <= height) and (height <= 76)

    # (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
    elif (field == "hcl"):
        b = re.search(hair_color_pattern, data)

    # ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
    elif (field == "ecl"):
        b = re.search(eye_color_pattern, data)

    # pid (Passport ID) - a nine-digit number, including leading zeroes.
    elif (field == "pid"):
        b = re.search(passport_id_pattern, data)

    return b


def partTwo():
    count_passports = 0
    fields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid", "cid"]
    line_pattern = re.compile("([a-z]+:\S*)")
    field_pattern = re.compile("([a-z]+):(\S*)")

    for i in range(0, len(passports)):
        match_fields = re.findall(line_pattern, passports[i])

        if (len(match_fields) > 0):
            for m in match_fields:
                s = re.search(field_pattern, m)

                f = s.group(1)
                data = s.group(2)

                if (f in fields) and (checkField(f, data)):
                    fields.remove(f)

        if (i == len(passports)-1) or (len(match_fields) == 0):
            # If the program finds an empty line
            # this means, it has finished parsing the passport

            if ((len(fields) == 0) or ((len(fields) == 1) and (fields[0] == "cid"))):
                count_passports += 1

            fields = ["byr", "iyr", "eyr", "hgt",
                      "hcl", "ecl", "pid", "cid"]

    message = "Part two: {0} valid passports found.".format(count_passports)

    if (count_passports < 2):
        message = message.replace("passports", "passport")

    print(message)


if __name__ == "__main__":
    partOne()
    partTwo()
