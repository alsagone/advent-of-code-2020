import sys

lines = open("../inputs/day-03.txt").read().splitlines()
line_length = len(lines[0])


def slide(right, down):
    count_trees = 0
    y = 0

    for x in range(0, len(lines), down):
        if (lines[x][y] == '#'):
            count_trees += 1

        y = (y+right) % line_length

    return count_trees


def partOne():
    print("Part one: {0} trees encountered".format(slide(3, 1)))


def partTwo():
    count_trees = slide(1, 1) * slide(3, 1) * \
        slide(5, 1) * slide(7, 1) * slide(1, 2)

    print("Part two: {0} trees encountered".format(count_trees))


if __name__ == "__main__":
    partOne()
    partTwo()
