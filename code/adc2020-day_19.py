import re

rulesDict = None
stringList = None


def getPuzzleInput():
    with open("../inputs/day-19.txt") as f:
        puzzleInput = f.read().splitlines()

    return puzzleInput


def parseInput(puzzleInput):
    global rulesDict
    rulesDict = {}
    global stringList
    stringList = []

    patternChars = re.compile(r"(\d+): \"([a-z])\"")
    patternNumbers = re.compile(r"(\d+)")
    pattern_AB = re.compile(r"^[ab]+$")

    for line in puzzleInput:
        if ':' in line:
            if ('"' in line):
                result = patternChars.search(line)
                ruleNumber = int(result.group(1))
                ruleChar = result.group(2)
                rulesDict[ruleNumber] = ruleChar

            else:
                result = patternNumbers.findall(line)
                ruleNumber = int(result[0])
                ruleIndexes = []

                prefixLength = len(result[0]) + 2
                lineWithoutPrefix = line[prefixLength:]

                for part in lineWithoutPrefix.split('|'):
                    numbers = patternNumbers.findall(part)
                    ruleIndexes.append([int(n) for n in numbers])

                rulesDict[ruleNumber] = ruleIndexes

        elif (pattern_AB.match(line)):
            stringList.append(line)

    return


def respectsRule(expression, stack):
    if (len(stack) > len(expression)):
        return False

    elif (len(stack) == 0 or len(expression) == 0):
        return len(stack) == len(expression)

    item = stack.pop()

    # If it's a rule we can directly evaluate, check if the first element of the expression matches the rule
    # If yes, continue with the rest of the expression
    if isinstance(item, str):
        if expression[0] == item:
            return respectsRule(expression[1:], stack.copy())

    # Else, add the new rules to the stack and continue
    else:
        for rule in rulesDict[item]:
            if (respectsRule(expression, stack + list(reversed(rule)))):
                return True

    return False


def partOne(puzzleInput):
    if (rulesDict == None or stringList == None):
        parseInput(puzzleInput)

    result = 0
    for expression in stringList:
        if respectsRule(expression, list(reversed(rulesDict[0][0]))):
            result += 1

    print(f'Part one: {result}')
    return


def partTwo(puzzleInput):
    if (rulesDict == None or stringList == None):
        parseInput(puzzleInput)

    rulesDict[8] = [[42], [42, 8]]
    rulesDict[11] = [[42, 31], [42, 11, 31]]

    result = 0
    for expression in stringList:
        if respectsRule(expression, list(reversed(rulesDict[0][0]))):
            result += 1

    print(f'Part two: {result}')
    return


if __name__ == "__main__":
    puzzleInput = getPuzzleInput()
    partOne(puzzleInput)
    partTwo(puzzleInput)
