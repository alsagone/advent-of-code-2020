import collections

answers = open("../inputs/day-06.txt").read().splitlines()
last_answer_index = len(answers) - 1


def partOne():
    group = ""
    count = 0

    for i in range(len(answers)):
        answer = answers[i]
        len_answer = len(answer)

        # We append each answer to a string (group) until we meet an empty line in the file
        # or reach the end of the file
        if (len_answer > 0):
            group += answer

        # collections.Counter(...) lists the number of occurences of each element in a list
        # In our case, this means that we can get the number of distinct answers in a group
        # with len(counter(group).items())
        if (len_answer == 0) or (i == last_answer_index):
            results = collections.Counter(group)
            count += len(results.items())
            group = ""

    print("Part one: {0}".format(count))


def partTwo():
    group = ""
    count = 0
    len_group = 0

    for i in range(len(answers)):
        answer = answers[i]
        len_answer = len(answer)

        if (len_answer > 0):
            group += answer
            len_group += 1

        if (len_answer == 0) or (i == last_answer_index):
            # If the group is composed of a single person, add the length of the answers to the counter
            if (len_group == 1):
                count += len(group)

            # Else, count the answers
            elif (len_group > 1):
                results = collections.Counter(group)

                # For each answer, if it's listed the same number of times as the length of the group
                # Then it means, everybody in the group answered yes to this question
                # For example, if 'a' is present 3 times and there are 3 people in the group
                # It means everybody answers yes to the question 'a'
                for v in results.values():
                    if (v == len_group):
                        count += 1

            # Resetting the group and its length
            group = ""
            len_group = 0

    print("Part two: {0}".format(count))


if __name__ == "__main__":
    partOne()
    partTwo()
