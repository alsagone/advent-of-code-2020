import sys


def getInput():
    input = []

    with open('../inputs/day-09.txt') as f:
        lines = f.read().splitlines()

    for l in lines:
        input.append(int(l))

    return input

# Gets the windowSize previous numbers, starting from the specified index
# and returns an array containing every sum between each pair on numbers


def getSums(array, index, windowSize):
    start = index - windowSize
    sliced_array = array[start:index]
    len_sliced_array = len(sliced_array)
    sums = []

    for i in range(0, len_sliced_array - 1):
        a = sliced_array[i]
        for j in range(i+1, len_sliced_array):
            sums.append(a + sliced_array[j])

    return sums


def partOne():
    input = getInput()
    incorrectNumber = -1
    windowSize = 25
    found = False

    for i in range(windowSize+1, len(input)):
        number = input[i]
        sums = getSums(input, i, windowSize)
        if (number not in sums):
            incorrectNumber = number
            found = True
            break

    if not(found):
        print("Error: incorrect number not found", file=sys.stderr)
        sys.exit(1)

    print(f"Part one: {incorrectNumber}")
    return incorrectNumber


def partTwo(targetNumber):
    input = getInput()
    maxIndex = input.index(targetNumber)
    array = []
    sum = 0
    answer = 0
    found = False

    start = 0
    while not(found) and (start < maxIndex):
        for i in range(start, maxIndex):
            number = input[i]
            if (sum + number <= targetNumber):
                sum += number
                array.append(number)
            elif (sum == targetNumber):
                answer = min(array) + max(array)
                found = True
                break
            else:
                sum = 0
                array = []

        if (found):
            break
        else:
            start += 1

    if not(found):
        print("Error: target number not found", file=sys.stderr)
        sys.exit(1)

    print(f"Part two: {answer}")


if __name__ == '__main__':
    targetNumber = partOne()
    partTwo(targetNumber)
