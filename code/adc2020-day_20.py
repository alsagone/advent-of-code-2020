import re
from functools import reduce


def getPuzzleInput():
    with open("../inputs/day-20.txt") as f:
        puzzleInput = f.read().splitlines()

    return puzzleInput


class Tile:
    def __init__(self, number, top, right, bottom, left) -> None:
        self.number = number
        self.borders = []

        for side in [top, right, bottom, left]:
            self.borders.append(side)
            # Reversed side in case the tile is flipped
            self.borders.append(side[::-1])

    def isNeighbor(self, otherTile):
        return (self.number != otherTile.number) and (set(self.borders) & set(otherTile.borders))


def getTile(number, tileContent):
    top = tileContent[0]
    bottom = tileContent[-1]

    left = ""
    right = ""

    for x in tileContent:
        left += x[0]
        right += x[-1]

    return Tile(number, top, right, bottom, left)


def parseInput(puzzleInput):
    tileArray = []
    patternNumbers = re.compile(r"(\d+)")
    patternTileContent = re.compile(r".|#")

    for line in puzzleInput:
        if ("Tile" in line):
            result = patternNumbers.findall(line)
            tileNumber = int(result[0])
            tileContent = []

        elif (patternTileContent.match(line) and (tileContent != None)):
            tileContent.append(line)

        elif (tileContent != None):
            tile = getTile(tileNumber, tileContent)
            tileArray.append(tile)
            tileContent = None

    return tileArray


def findNeighbors(tileArray):
    neighborsDict = {}

    for tile in tileArray:
        neighborsIDs = []
        for otherTile in tileArray:
            if tile.isNeighbor(otherTile):
                neighborsIDs.append(otherTile.number)

        neighborsDict[tile.number] = neighborsIDs

    return neighborsDict

# We don't have to reconstruct the full puzzle
# We have only have to find the tiles on the 4 corners
# We can easily detect them because those tiles only have two neighbors


def partOne(tileArray):
    tileArray = parseInput(puzzleInput)
    neighborsDict = findNeighbors(tileArray)
    cornerIDs = []

    for (tileNumber, neighborsList) in neighborsDict.items():
        if (len(neighborsList) == 2):
            cornerIDs.append(tileNumber)

    result = reduce((lambda x, y: x * y), cornerIDs)
    print(f'Part one: {result}')
    return


if __name__ == "__main__":
    puzzleInput = getPuzzleInput()
    tileArray = parseInput(puzzleInput)
    partOne(tileArray)
