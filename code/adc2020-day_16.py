from functools import reduce
import re


def isBetween(x: int, minVal: int, maxVal: int) -> bool:
    assert minVal <= maxVal
    return (minVal <= x) and (x <= maxVal)


def getPuzzleInput() -> list[str]:
    with open("../inputs/day-16.txt") as f:
        puzzleInput = f.read().splitlines()

    return puzzleInput


def getRulesDict(rules: list[str]) -> dict[list[tuple]]:
    rulesDict = {}

    for r in rules:
        ruleName = r.split(":")[0]
        matches = re.findall(r'\d+', r)

        if (not "or" in r) or (len(matches) != 4):
            raise Exception(f'Incorrect rule: "{r}"')

        numbers = [int(x) for x in matches]

        rulesDict[ruleName] = [(numbers[0], numbers[1]),
                               (numbers[2], numbers[3])]

    return rulesDict


def respectsRule(value: int, rule: list[tuple]) -> bool:
    valid = False
    for tuple in rule:
        if (isBetween(value, tuple[0], tuple[1])):
            valid = True
            break

    return valid


def getErrorRate(ticket: str, rulesList: list[list[tuple]]) -> int:
    ticketNumbers = [int(x) for x in ticket.split(',')]

    errorRate = 0
    for number in ticketNumbers:
        respectsOneRule = False
        for rule in rulesList:
            if respectsRule(number, rule):
                respectsOneRule = True
                break

        if (not(respectsOneRule)):
            errorRate += number

    return errorRate


def partOne(puzzleInput: list[str]):
    emptyLinesIndex = [i for i in range(
        len(puzzleInput)) if puzzleInput[i] == '']

    rules = puzzleInput[0:emptyLinesIndex[0]]
    nearbyTickets = puzzleInput[emptyLinesIndex[1]+2:]

    rulesList = list(getRulesDict(rules).values())
    errorRate = 0

    for t in nearbyTickets:
        errorRate += getErrorRate(t, rulesList)

    print(f"Part one: {errorRate}")
    return errorRate


def getValidTickets(ticketsList: list[str], rulesDict: dict[list[tuple]]) -> list[str]:
    validTickets = []
    rulesList = list(rulesDict.values())

    for ticket in ticketsList:
        if (getErrorRate(ticket, rulesList) == 0):
            validTickets.append(ticket)

    return validTickets


def getRuleIndex(ruleName: str, rulesDict: dict[list[tuple]]) -> int:
    index = -1

    for idx, value in enumerate(list(rulesDict.keys())):
        if (value == ruleName):
            index = idx

    return index


def getFieldAt(index: int, fieldDict: dict[int]) -> str:
    try:
        field = list(fieldDict.keys())[index]

    except IndexError:
        raise Exception("Index out of range")

    return field


def respectsAllRules(ticketsList: list[str], index: int, rule: list[tuple]):
    '''
    Checks if the index-th values of each ticket respects the rule
    '''
    values = []
    for t in ticketsList:
        split = t.split(',')
        values.append(int(split[index]))

    b = True

    for v in values:
        if not(respectsRule(v, rule)):
            b = False
            break

    return b

# Returns a dict that associates an index N with every Nth number of each ticket


def splitTicketValues(ticketsList: list[str]):
    ticketsDict = {}

    for ticket in ticketsList:
        ticketValues = [int(x) for x in ticket.split(",")]

        for index, value in enumerate(ticketValues):
            idx = index + 1

            if (idx in ticketsDict.keys()):
                ticketsDict[idx].append(value)

            else:
                ticketsDict[idx] = [value]

    return ticketsDict


def determineFields(ticketsList: list[str], rulesDict: dict[list[tuple]]):
    possibleFields = {}
    ticketsDict = splitTicketValues(ticketsList)

    for ruleName in list(rulesDict.keys()):
        possibleFields[ruleName] = []

    rulesList = list(rulesDict.values())

    for ruleIndex, rule in enumerate(rulesList):
        fieldName = getFieldAt(ruleIndex, possibleFields)

        for fieldNumber, values in ticketsDict.items():
            ruleValid = True

            for v in values:
                if not(respectsRule(v, rule)):
                    ruleValid = False
                    break

                if not(ruleValid):
                    break

            if ruleValid:
                if (fieldNumber not in possibleFields[fieldName]):
                    possibleFields[fieldName].append(fieldNumber)

    """
    While there are fields that have more than one possibility:
        For all possible fields:
            - Take the one that has only one possibility 
            - Delete this possibility from the other fields 
    """

    uniqueFields = False

    while not(uniqueFields):
        uniqueFields = True

        for (fieldName, fieldIndexes) in possibleFields.items():
            if (len(fieldIndexes) == 1):
                fieldToDelete = fieldIndexes[0]

                for (key, value) in possibleFields.items():
                    if (key != fieldName):
                        possibleFields[key] = [
                            x for x in value if x != fieldToDelete]

            elif (len(fieldIndexes) > 1):
                uniqueFields = False

    return possibleFields


def partTwo(puzzleInput: list[str]):
    i = 0
    rules = []
    while (puzzleInput[i] != ''):
        rules.append(puzzleInput[i])
        i += 1

    rulesDict = getRulesDict(rules)

    ticketsList = []
    for line in puzzleInput[i+2:]:
        if "," in line:
            ticketsList.append(line)

    myTicketIndex = puzzleInput.index("your ticket:")+1
    myTicket = [int(x) for x in puzzleInput[myTicketIndex].split(",")]

    validTickets = getValidTickets(ticketsList, rulesDict)

    fieldsDict = determineFields(validTickets, rulesDict)

    departureValues = []
    for field, value in fieldsDict.items():
        if field.startswith("departure"):
            departureValues.append(myTicket[value[0]-1])

    result = reduce(lambda a, b: a*b, departureValues)

    print(f"Part two: {result}")
    return result


if __name__ == '__main__':
    puzzleInput = getPuzzleInput()
    assert partOne(puzzleInput) == 22073
    assert partTwo(puzzleInput) == 1346570764607
