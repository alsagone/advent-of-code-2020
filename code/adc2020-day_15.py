
from time import time


def game(startingNumbers: list[int], nbTurns: int) -> int:
    # dict with the number and the Nth turn where it was spoken for the last time
    dictTurns = {number: turn for turn,
                 number in enumerate(startingNumbers)}

    lastSpokenNumber = startingNumbers[-1]
    for turn in range(len(startingNumbers), nbTurns):
        try:
            # if the last spoken number already exists in the dict
            nextValue = turn - 1 - dictTurns[lastSpokenNumber]

        except KeyError:
            # if it doesn't
            nextValue = 0

        dictTurns[lastSpokenNumber] = turn - 1
        lastSpokenNumber = nextValue

    return lastSpokenNumber


def getPuzzleInput() -> list[str]:
    with open("../inputs/day-15.txt") as f:
        firstline = f.readline().rstrip()

    return [int(x) for x in firstline.split(',')]


def partOne(puzzleInput: list[int]) -> None:
    lastSpokenNumber = game(puzzleInput, 2020)
    print(f"Part one: {lastSpokenNumber}")
    return


def partTwo(puzzleInput: list[int]) -> None:
    startTime = time()
    lastSpokenNumber = game(puzzleInput, 30000000)
    executionTime = time() - startTime
    print(f"Part two: {lastSpokenNumber}")
    print(f"--- Part 2 execution time: {executionTime:.2f} seconds ---")
    return


if __name__ == '__main__':
    puzzleInput = getPuzzleInput()
    partOne(puzzleInput)
    partTwo(puzzleInput)
