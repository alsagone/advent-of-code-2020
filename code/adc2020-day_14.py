import re
from functools import reduce
from itertools import product


def decimalNumberToBinaryString(decimalValue: int, nbBits: int) -> str:
    ''' Convert an integer number to a binary string with a defined number of bits'''
    binValue = bin(decimalValue)[2:]
    # 2: removes the '0b' prefix that bin(x) adds

    # Checks if there is padding 0's to add
    paddingLength = nbBits - len(binValue)
    if (paddingLength > 0):
        binValue = '0' * paddingLength + binValue

    return binValue


def getMemoryValues(memStr: str) -> int:
    '''Return the index and the value of a memory instruction
    Example: getMemoryValues(mem[42] = 100) returns 42 and 100'''
    matches = re.findall(r'\d+', memStr)

    if (len(matches) != 2):
        print(matches)
        raise Exception("Error memory regex")

    return int(matches[0]), int(matches[1])


def getMask(maskStr: str) -> str:
    '''Returns the mask from a mask instruction'''
    match = re.findall(r'[10X]+', maskStr)

    if (len(match) != 1):
        raise Exception("Error mask regex")

    return match[0]


def combineWithMask(binaryStr: str, mask: str) -> int:
    '''Combines each bit of the binary string with the mask and returns the result as an integer'''
    if (len(mask) != 36):
        raise Exception(f"Incorrect mask: '{mask}'")

    binaryResult = ''
    appendValue = ''
    for i in range(35, -1, -1):
        if (mask[i] == 'X'):
            appendValue = binaryStr[i]

        elif (mask[i] == '0'):
            appendValue = '0'

        elif (mask[i] == '1'):
            appendValue = '1'

        else:
            raise Exception(f"Unknown mask character ${mask[i]}")

        binaryResult = appendValue + binaryResult

    return int(binaryResult, 2)


def getInput():
    with open('../inputs/day-14.txt') as f:
        lines = f.read().splitlines()

    return lines


def executeInstructions(input: list[str]) -> dict:
    memory = {}
    mask = 'X' * 36

    for line in input:
        if (line.startswith('mask')):
            mask = getMask(line)

        elif (line.startswith('mem')):
            index, decimalValue = getMemoryValues(line)
            binaryStr = decimalNumberToBinaryString(decimalValue, 36)
            newValue = combineWithMask(binaryStr, mask)
            memory[index] = newValue

        else:
            raise Exception(f"Unknown prefix: {line}")

    return memory


def partOne(input: list[str]):
    memory = executeInstructions(input)
    sumMemory = reduce(lambda a, b: a+b, list(memory.values()))
    print(f"Part one: {sumMemory}")
    return


def findAllIndexes(char: str, string: str):
    '''Returns a list of every index of the specified char in the string'''
    return [i for i, c in enumerate(string) if c == char]


def getAllBitsCombinaisons(nbBits: int) -> list[list[int]]:
    '''Returns a list of lists with every combinaison of 1's and 0's on n bits
    For example with n = 3, [ [0,0,0], [0,0,1], [0,1,0], ... , [1,1,1] ]'''
    return [list(i) for i in product([0, 1], repeat=nbBits)]


def replaceCharAtIndex(string: str, index: int, newChar: str):
    ''' Replace character at index in string with the
    given replacement character.'''
    newStr = ''
    if index < len(string):
        newStr = string[0:index] + newChar + string[index + 1:]
    else:
        raise Exception("Index out of range")

    return newStr


def getAdresses(floatingString: str):
    list = []

    # Find all the indexes of the 'X' in the string
    floatingBitsIndexes = findAllIndexes('X', floatingString)
    nbFloatingBits = len(floatingBitsIndexes)

    bitsCombinaisons = getAllBitsCombinaisons(nbFloatingBits)

    for combinaison in bitsCombinaisons:
        newStr = floatingString
        for i in range(nbFloatingBits):
            # Transform every 'X' of the string into a 0 or a 1 according to the combinaison
            newStr = replaceCharAtIndex(
                newStr, floatingBitsIndexes[i], str(combinaison[i]))

        list.append(int(newStr, 2))

    return list


def combineWithMask_v2(binaryStr: int, mask: str) -> list[int]:
    if (len(mask) != 36):
        raise Exception(f"Incorrect mask: '{mask}'")

    binaryResult = ''
    appendValue = ''
    for i in range(35, -1, -1):
        if (mask[i] == '0'):
            appendValue = binaryStr[i]

        elif (mask[i] == 'X'):
            appendValue = 'X'

        elif (mask[i] == '1'):
            appendValue = '1'

        else:
            raise Exception(f"Unknown mask character ${mask[i]}")

        binaryResult = appendValue + binaryResult

    return getAdresses(binaryResult)


def executeInstructions_v2(input: list[str]) -> dict:
    memory = {}
    mask = 'X' * 36

    for line in input:
        if (line.startswith('mask')):
            mask = getMask(line)

        elif (line.startswith('mem')):
            index, decimalValue = getMemoryValues(line)
            binaryStr = decimalNumberToBinaryString(index, 36)
            addressList = combineWithMask_v2(binaryStr, mask)

            for address in addressList:
                memory[address] = decimalValue

        else:
            raise Exception(f"Unknown prefix: {line}")

    return memory


def partTwo(input: list[str]):
    memory = executeInstructions_v2(input)
    sumMemory = reduce(lambda a, b: a+b, list(memory.values()))
    print(f"Part two: {sumMemory}")
    return


if __name__ == '__main__':
    input = getInput()
    partOne(input)
    partTwo(input)
