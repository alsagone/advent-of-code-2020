import sys
seats = open("../inputs/day-05.txt").read().splitlines()


def splitList(my_list, part):
    middle_index = len(my_list)//2
    if (part == "upper"):
        splitted_list = my_list[middle_index:]
    elif (part == "lower"):
        splitted_list = my_list[:middle_index]

    else:
        print("Unknown operation ({0})".format(part))
        sys.exit(1)

    return splitted_list, splitted_list[0], splitted_list[len(splitted_list)-1]


def findSeat(string):

    row_index = [i for i in range(128)]
    col_index = [j for j in range(8)]

    min_row_index = 0
    max_row_index = 127

    min_col_index = 0
    max_col_index = 7

    row_string = string[:-3]
    col_string = string[-3:]

    limit_row = len(row_string) - 1
    limit_col = len(col_string) - 1

    for i in range(len(row_string)):
        c = row_string[i]

        # F -> lower half
        if (c == 'F'):
            if (i != limit_row):
                row_index, min_row_index, max_row_index = splitList(
                    row_index, "lower")

            else:
                final_row_index = min_row_index
        # B -> upper half
        elif (c == 'B'):
            if (i != limit_row):
                row_index, min_row_index, max_row_index = splitList(
                    row_index, "upper")
            else:
                final_row_index = max_row_index

        else:
            print("Error: unknown character ({0})".format(c), file=sys.stderr)
            sys.exit(1)

    for j in range(len(col_string)):
        c = col_string[j]

        # L -> lower half
        if (c == 'L'):
            if (j != limit_col):
                col_index, min_col_index, max_col_index = splitList(
                    col_index, "lower")
            else:
                final_col_index = min_col_index

        elif (c == 'R'):
            if (j != limit_col):
                col_index, min_col_index, max_col_index = splitList(
                    col_index, "upper")
            else:
                final_col_index = max_col_index

        else:
            print("Error: unknown character ({0})".format(c), file=sys.stderr)
            sys.exit(1)

    return 8 * final_row_index + final_col_index


seat_ids = [findSeat(s) for s in seats]
seat_ids.sort()


def partOne():
    maximum_value = seat_ids[-1:][0]
    print("Part one - maximum value: {0}".format(maximum_value))


# Find the first non-consecutive seat id in the list and add 1
def partTwo():
    for i in range(len(seat_ids)-1):
        if (seat_ids[i]+1 != seat_ids[i+1]):
            my_seat_id = seat_ids[i] + 1
            print("Part two - your seat ID is {0}".format(my_seat_id))
            break


if __name__ == "__main__":
    partOne()
    partTwo()
