import sys


def getInput():
    """
    Returns an array containing each line of the input file
    """
    with open('../inputs/day-08.txt') as f:
        lines = f.read().splitlines()

    return lines


def parseInput(lines):
    """
    Returns an array of tuples where each line of the input is parsed into a tuple (command, value)
    :param lines: An array containing each line of the input file (from the getInput() function)
    """
    input = []

    for l in lines:
        split = l.split(' ')
        input.append((split[0], split[1]))

    return input


def getCommandAndValue(instruction):
    split = instruction.split(' ')
    return split[0], int(split[1])


def executeInstructions_partOne(input):
    index = 0
    accumulator = 0
    stored_index = []
    done = False

    while not(done):
        if not(index in stored_index):
            stored_index.append(index)
            command, value = input[index][0], int(input[index][1])

            if (command == "nop"):
                index += 1

            elif (command == "acc"):
                accumulator += value
                index += 1

            elif (command == "jmp"):
                index += value

            else:
                print(f"Unknown instruction: {command}", file=sys.stderr)
                sys.exit(1)
        else:
            done = True

    return accumulator


def executeInstructions_partTwo(input):
    index = 0
    accumulator = 0
    stored_index = []
    done = False
    nbInstructions = len(input)
    limit = nbInstructions - 1

    while not(done):
        if (index <= limit) and not(index in stored_index):
            stored_index.append(index)
            command, value = input[index][0], int(input[index][1])

            if (command == "nop"):
                index += 1

            elif (command == "acc"):
                accumulator += value
                index += 1

            elif (command == "jmp"):
                index += value

            else:
                print(f"Unknown instruction: {command}", file=sys.stderr)
                sys.exit(1)
        else:
            done = True

    exitsNaturally = (index == nbInstructions)

    return accumulator, exitsNaturally


def partOne():
    input = parseInput(getInput())
    accumulator = executeInstructions_partOne(input)
    print(f"Part one: {accumulator}")
    return


def partTwo():
    inputStr = getInput()
    accumulator = 0
    found = False

    # Indexes of nop instructions
    nop_and_jmp = []

    for i in range(len(inputStr)):
        instruction = inputStr[i]
        if ("nop" in instruction) or ("jmp" in instruction):
            nop_and_jmp.append(i)

    # For each nop and jmp instruction, we try to switch it from nop to jmp or jmp to nop
    # execute the instructions and check if the programs exits naturally
    for index in nop_and_jmp:
        inputStr_copy = inputStr[:]

        command, value = getCommandAndValue(inputStr_copy[index])
        if (command == "nop") and (value != 0):
            inputStr_copy[index] = inputStr_copy[index].replace(command, "jmp")

        elif (command == "jmp"):
            inputStr_copy[index] = inputStr_copy[index].replace(command, "nop")

        input = parseInput(inputStr_copy)
        accumulator, exitsNaturally = executeInstructions_partTwo(input)

        if (exitsNaturally):
            found = True
            break

    if not(found):
        print(
            "Error: the correct instruction to replace has not been found", file=sys.stderr)
        exit(1)

    print(f"Part two: {accumulator}")


if __name__ == "__main__":
    input = getInput()
    partOne()
    partTwo()
