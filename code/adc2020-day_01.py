import sys


def part1():
    numbers = open("../inputs/day-01.txt").read().splitlines()
    found = False

    for i in range(0, len(numbers)-1, 1):
        a = int(numbers[i])

        for j in range(i+1, len(numbers), 1):
            b = int(numbers[j])

            if (a+b == 2020):
                result = a*b
                print("{0} + {1} = 2020".format(a, b))
                print("{0} * {1} = {2}".format(a, b, result))
                found = True
                return

    if not(found):
        print("Error: sum not found", file=sys.stderr)


def part2():
    numbers = open("../inputs/day-01.txt").read().splitlines()
    found = False

    for i in range(0, len(numbers)-2, 1):
        a = int(numbers[i])

        for j in range(i+1, len(numbers)-1, 1):
            b = int(numbers[j])

            for k in range(j+1, len(numbers), 1):
                c = int(numbers[k])

                if (a+b+c == 2020):
                    result = a*b*c
                    print("{0} + {1} + {2} = 2020".format(a, b, c))
                    print("{0} * {1} * {2} = {3}".format(a, b, c, result))
                    found = True
                    return

    if not(found):
        print("Error: sum not found", file=sys.stderr)


if __name__ == "__main__":
    part1()
    part2()
