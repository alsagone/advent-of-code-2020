import sys
import re


def checkPassword_partOne(string):
    s = re.search(r"^([0-9]+)-([0-9]+) ([a-z]): ([a-z]+)$", string)

    if (s == None):
        print("Error: the pattern doesn't match (\"{0}\")".format(string))
        return False

    min_char = int(s.group(1))
    max_char = int(s.group(2))
    char = s.group(3)
    password = s.group(4)

    count_char = password.count(char)
    return (min_char <= count_char) and (count_char <= max_char)


def checkPassword_partTwo(string):
    s = re.search(r"^([0-9]+)-([0-9]+) ([a-z]): ([a-z]+)$", string)

    if (s == None):
        print("Error: the pattern doesn't match (\"{0}\")".format(string))
        return False

    first_index = int(s.group(1)) - 1
    second_index = int(s.group(2)) - 1
    char = s.group(3)
    password = s.group(4)

    try:
        check_first = password[first_index] == char
        check_second = password[second_index] == char
        b = (check_first or check_second) and not(check_first and check_second)

    except:
        b = False

    return b


def part1():
    passwords = open("../inputs/day-02.txt").read().splitlines()
    count = 0

    for p in passwords:
        if (checkPassword_partOne(p)):
            count += 1

    if (count > 1):
        message = "{0} passwords are valid. (part 1)".format(count)
    else:
        message = "{0} password is valid. (part 1)".format(count)

    print(message)


def part2():
    passwords = open("../inputs/day-02.txt").read().splitlines()
    count = 0

    for p in passwords:
        if (checkPassword_partTwo(p)):
            count += 1

    if (count > 1):
        message = "{0} passwords are valid. (part 2)".format(count)
    else:
        message = "{0} password is valid. (part 2)".format(count)

    print(message)


if __name__ == "__main__":
    part1()
    part2()
