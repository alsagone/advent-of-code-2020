

operatorsList = ['+', '-', '*', '/', '^']


def getPuzzleInput() -> list[str]:
    with open("../inputs/day-18.txt") as f:
        puzzleInput = f.read().splitlines()

    return puzzleInput


def isOperator(item):
    return item in operatorsList


def getNestedBracketsIndexes(exprList):
    leftBracketIndex = -1
    rightBracketIndex = -1

    for (index, value) in enumerate(exprList):
        if (value == '('):
            leftBracketIndex = max(leftBracketIndex, index)

        elif (value == ')'):
            rightBracketIndex = index
            break

    if (leftBracketIndex == -1):
        raise f'No left bracket found in {"".join(exprList)}'

    if (rightBracketIndex == -1):
        raise f'Matching right bracket not found in {"".join(exprList)}'

    return leftBracketIndex, rightBracketIndex


def evaluateSimple(exprList):
    result = None
    operator = None

    for item in exprList:
        if isOperator(item):
            operator = item

        else:
            value = int(item)

            if operator == None:
                result = value

            elif operator == '+':
                result += value

            elif operator == '*':
                result *= value

            else:
                raise f'Bad item: {item}'

    return result


def evaluate(expression):
    exprList = [x for x in expression.replace(" ", "")]

    while '(' in exprList:
        # Get the indexes of the left and right brackets of the deepest matching brackets
        leftBracketIndex, rightBracketIndex = getNestedBracketsIndexes(
            exprList)

        # Get what's between those brackets
        subExpression = exprList[leftBracketIndex+1:
                                 rightBracketIndex]

        # Replace the left bracket on the original expression by the result of the sub expression
        exprList[leftBracketIndex] = str(evaluateSimple(subExpression))

        # Delete the rest of the sub expression
        del exprList[leftBracketIndex+1:rightBracketIndex+1]

    # No brackets in the expression -> evaluate
    return evaluateSimple(exprList)


def evaluateSimple_v2(exprList):
    while ('+' in exprList):
        # Take the first '+' in the expression
        plusIndex = exprList.index('+')

        # Get what's left and right of the '+'
        subExpression = exprList[plusIndex-1:plusIndex+2]

        # Replace the left item on the original expression by the result of the sub expression
        exprList[plusIndex-1] = str(evaluateSimple(subExpression))

        # Delete the rest of the sub expression
        del exprList[plusIndex:plusIndex+2]

    return evaluateSimple(exprList)


def evaluate_v2(expression):
    exprList = [x for x in expression.replace(" ", "")]

    while '(' in exprList:
        # Get the indexes of the left and right brackets of the deepest matching brackets
        leftBracketIndex, rightBracketIndex = getNestedBracketsIndexes(
            exprList)

        # Get what's between those brackets
        subExpression = exprList[leftBracketIndex+1:
                                 rightBracketIndex]

        # Replace the left bracket on the original expression by the result of the sub expression
        exprList[leftBracketIndex] = str(evaluateSimple_v2(subExpression))

        # Delete the rest of the sub expression
        del exprList[leftBracketIndex+1:rightBracketIndex+1]

    # No brackets in the expression -> evaluate
    return evaluateSimple_v2(exprList)


def partOne(puzzleInput):
    result = sum(evaluate(expression) for expression in puzzleInput)
    print(f"Part one: {result}")


def partTwo(puzzleInput):
    result = sum(evaluate_v2(expression) for expression in puzzleInput)
    print(f"Part two: {result}")


if __name__ == "__main__":
    puzzleInput = getPuzzleInput()
    partOne(puzzleInput)
    partTwo(puzzleInput)
