import numpy as np


def getInput():
    input = []

    with open('../inputs/day-10.txt') as f:
        lines = f.read().splitlines()

    for l in lines:
        input.append(int(l))

    input.insert(0, 0)

    sorted_input = np.sort(input, kind="mergesort")

    maximum = sorted_input[-1]

    return np.append(sorted_input, maximum+3)


def partOne():
    sorted_input = getInput()

    gapOne = 0
    gapThree = 0

    for i in range(0, len(sorted_input)-1):
        diff = sorted_input[i+1] - sorted_input[i]

        if (diff == 1):
            gapOne += 1

        elif (diff == 3):
            gapThree += 1

    answer = gapOne * gapThree
    print(f"Part one: {gapOne} * {gapThree} = {answer}")


def countPathsUtil(index, paths_dict, sorted_input, input_length):
    number = sorted_input[index]
    valid_numbers = [number + x for x in range(1, 4)]
    count = 0

    successors = sorted_input[index+1:input_length]
    if (len(successors) == 0):
        count = 1

    else:
        for n in valid_numbers:
            if (n in successors):
                count += paths_dict[n]

    if (index == 0):
        return count

    paths_dict[number] = count
    return countPathsUtil(index-1, paths_dict, sorted_input, input_length)


def countPaths(sorted_input):
    paths_dict = {}
    input_length = 0
    for number in sorted_input:
        paths_dict[number] = 0
        input_length += 1

    return countPathsUtil(input_length - 1, paths_dict, sorted_input, input_length)


def partTwo():
    sorted_input = getInput()
    nbPaths = countPaths(sorted_input)
    print(f"Part two: {nbPaths}")


if __name__ == '__main__':
    partOne()
    partTwo()
